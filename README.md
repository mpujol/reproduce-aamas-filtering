# Evaluation: Communication-constrained DCOPs: Message approximation in GDL with function filtering

This repository contains the information required to reproduce the results (and the data/graphs as shown in the paper).

## Solver and algorithms

The paper's experiments were conducted using `GDLFiltering` solver which can be [downloaded from github](https://github.com/kilburn/GDLFiltering). See the instructions in the included `README.md` file on how to build the solver and use it to test the different algorithms (and combinations). For this particular paper, the solver arguments used for each of the presented algorithms are:

- **GDL** (`-a gdl -s min`. Use standard `GDL` to minimize the problem).

- **Scope based** (`--compress=arith -a gdlf -s min -C bottom-up -M scope-based`):
  This defines a bottom-up method that uses the scope-based strategy to select which incoming functions to combine.

- **Content based** (`--compress=arith -a gdlf -s min -C bottom-up -M content-based --metric norm1`):
  Bottom-up method using the content-based strategy with the *LRE* metric. Change the metric parameter to `norminf` to test *LMRE*.

- **Brute force** (`--compress=arith -a gdlf -s min -C top-down -S brute-force --metric norm1`):
  Top-down method with brute-force decomposition using the *LRE* metric. We did not observe significant differences when trying other metrics.

- **Zero tracking** (`--compress=arith -a gdlf -s min -C top-down -S zerod`):
  Top-down method with zero-tracking decomposition.

Notice that, since all these algorithms employ a junction tree to operate, we pre-generated the junction trees for each of the tested problems and ran the different algorithms over the same junction tree. This can be accomplished by using the `--export-tree=<file>` option to generate the trees, and then adding `--load-tree=<file>` when running each algorithm.

## Datasets

The datasets we generated for these experiments are fairly large, and include hard problems that we were not able to solve. Nonetheless, the complete datasets are available in a separate location due to their size: [http://www.iiia.csic.es/~mpujol/papers/filtering-aamas/problems.tar.bz2](http://www.iiia.csic.es/~mpujol/filtering-aamas/problems.tar.bz2). Unzip that package and it will create a new `problems` folder with two datasets:

- **`rnd`**: problems with random structures of varying densities, numbers of variables, and variable arities.
- **`Lattice2D`**: regular lattice-structured problems

## Results

The results are reproducible by running the solver on the input problems and with the adequate settings commented above. However, doing so requires a hefty amount of time, and hence we also compiled all our raw results at [http://www.iiia.csic.es/~mpujol/papers/filtering-aamas/results.tar.bz2](http://www.iiia.csic.es/~mpujol/papers/filtering-aamas/problems.tar.bz2). Unzip that folder to obtain a `results` folder with all the results employed in the paper.

After running all the experiments (or fetching the results from the above location), you can reproduce the aggregate results and graphs used in the paper by running the `make_plots.sh` script in the `scripts` folder.
