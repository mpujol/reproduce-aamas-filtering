#!/usr/bin/python
# -*- coding: utf-8 -*-

import scipy.io 
import pickle
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss
import re
from pylab import *

# Parse the graph type
f = open('/tmp/data.txt', 'r')
line = f.readline()
m = re.match('Processing (.+)', line)
graph_key = m.group(1)
graph_titles = dict({
	'BYTES' : 'Overal communication savings',
	'TIME'  : 'Overall computation cost',
	'MEM'   : 'Maximum memory used (by the simulator, not by a single agent)',
})
graph_ops = dict({
	'BYTES' : lambda x: 1/x[f],
	'TIME'  : lambda x: x[f],
	'MEM'   : lambda x: x[f],
})
graph_yaxis = dict({
	'BYTES' : 'Savings wrt GDL',
	'TIME'  : 'Ratio wrt GDL',
	'MEM'   : 'Ratio wrt GDL',	
})

names = ('scpcc', 'scpcc10', 'lrecc', 'lrecc10', 'scpc', 'scpc10', 'lrec', 'lrec10', 'zerosd', 'zerosd10')
names = ('scpc', 'lrec', 'lmrec', 'lred', 'lmred', 'zerosd')
extended_names = ('Scope based', 'Content based', 'Brute force', 'Zero tracking')

data = np.genfromtxt('/tmp/data.txt', names=True, skip_header=1, converters={0: lambda s: int(s or 0)})
names = [name for name in names if name in data.dtype.names[3:]]
data = np.sort(data, order='CV')
#data = data[40:60];
width = 1/ float(len(names)+1)
#colors = [(43/255.,85/255.,162/255.), (90/255.,158/255.,221/255.), (0,172/255.,236/255.), (9/255.,0,75/255.), (.34, .66, .74),
colors = [(43/255.,85/255.,162/255.), (1,1,1), (0,172/255.,236/255.), (9/255.,0,75/255.), (.34, .66, .74),
(.87, .59, .32), (.65, .72, .85), (.85, .64, .64), (.77, .84, .65), (.72, .67, .79), (.65, .81, .87)]
edges = [(.61, .27, .26), (.5, .6, .31), (.28, .42, .6), (.47, .37, .57), (.23, .50, .57), 
(.64, .53, .32), (.60, .65, .72), (.73, .6, .6), (.68, .72, .61), (.65, .62, .69), (.61, .7, .74)]
hatches = ['', '', '', '', 'x', '\\', '-', '+', 'o', 'O', '.'];

clique_variables = np.unique(data['CV'])
means = []
stds = []
q1s = []
q3s = []
for cv in clique_variables:
	if cv < 5:
		continue
	els = data.compress(data['CV'] == cv)
	if len(els) < 5:
		continue
	mean = [0, cv, 0];
	std = [0, cv, 0];
	q1 = [0, cv, 0];
	q3 = [0, cv, 0];
	for f in range(3, len(els[0])):
		b = map(graph_ops[graph_key], els)
		mean.append(np.median(b))
		std.append(np.std(b))
		q1.append(np.median(b) - ss.scoreatpercentile(b, 25))
		q3.append(ss.scoreatpercentile(b, 75)- np.median(b))
		
	means.append(tuple(mean))
	stds.append(tuple(std))
	q1s.append(tuple(q1))
	q3s.append(tuple(q3))
	
means = np.array(means, dtype=data.dtype)
stds = np.array(stds, dtype=data.dtype)
q1s = np.array(q1s, dtype=data.dtype)
q3s = np.array(q3s, dtype=data.dtype)
lefts = np.arange(len(means['Problem']))
q = np.zeros((2,len(q1s)), dtype=data.dtype)
q[0] = q1s;
q[1] = q3s;

p = []
i = 0
for name in names:
	p.append(plt.bar(width/2.+ lefts+(i*width), means[name], width, color=colors[i], 
					  ec=(0,0,0), yerr=q[name], ecolor=(.3,.3,.3), hatch=hatches[i]))
	i = i + 1

plt.xticks(lefts+width*len(names)/2., means['CV'])
ps = []
for pi in p:
	ps.append(pi[0])
#plt.figlegend( tuple(ps), tuple(extended_names), loc='upper center', ncol=2)

plt.subplots_adjust(bottom=0.15, right=0.98, left=0.11)
xlabel('Maximum clique variables')
ylabel(graph_yaxis[graph_key])
title(graph_titles[graph_key])
ymin, ymax = ylim()
ylim(0, ymax);
fig = gcf()
DefaultSize = fig.get_size_inches()
fig.set_size_inches(DefaultSize[0]*1*2/3, DefaultSize[1]*0.75*2/3)
#show()
savefig('/tmp/graph.pdf')
