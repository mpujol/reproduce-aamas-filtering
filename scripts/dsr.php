#!/usr/bin/php
<?php
/************************************************************************
/*  
/* STATS GATHERING SCRIPT
/*
/* This script processes an arbitrary number of result files and outputs
/* per-algorithm results in a tabular format for easier further processing.
/*
/* Usage: dsr.php <stat> [directory]
/*
/* Parameters:
/*    stat       The output statistic to track. Usually 'BYTES' or 'TIME'.
/*    directory  Directory that contains the result files to process.
/*
/* Sample output:
/*
/*    Processing TIME
/*    Problem    CV           gdl        lre-c        lre-d        scp-c      zeros-d
/*    rnd.nv=20.p=0.2.weights=normal.maxArity=5.29 2 97.60 21.191189 147.367013 2.900717 7.431659    
/*    rnd.nv=20.p=0.2.weights=normal.maxArity=5.49 2 171.67 13.453428 47.363372 3.947690 15.534281    
/*    rnd.nv=20.p=0.2.weights=normal.maxArity=5.5 2 346.67 17.353160 - 5.274353 12.659128    
/* 
/* Output description:
/*    The 1st line indicates that these are 'TIME' statistics.
/*    The 2nd line are column labels. 'CV' are the maximum clique variables, and later columns
/*      represent different algorithms.
/*    From the 3rd line to the end are the actual results. Each line is a problem and shows the
/*      results obtained by each algorithm when solving that problem
/*
/************************************************************************/

$pivot = 'gdl';
$field = empty($argv[1]) ? 'BYTES' : $argv[1];
$dir = empty($argv[2]) ? '.' : $argv[2];

$data = getFiles($dir);
$algos = array_keys($data);
array_shift($algos);

// remove the pivot
$key = array_search($pivot, $algos);
unset($algos[$key]);

printf("Processing " . strtoupper($field) . "\n");
printf("%-7s %5s %13s", 'Problem', 'CV', $pivot);
foreach($algos as $algo) {
	printf("%13s", $algo);
}
printf("\n");

foreach($data['problems'] as $problem) {
	// Non-solved problems
	$v = $data[$pivot][$problem][$field];
	$cs = $data[$pivot][$problem]['MAX_CLIQUE_VARIABLES'];
	if ((float)$v == 0) {
		printf("%-7s %5s %13s", $problem, $cs, '-');
	} else {
		printf("%-7s %5s %13.2f", $problem, $cs, $v);
	}
	foreach($algos as $algo) {
		$v2 = (float)$data[$algo][$problem][$field];
		if ($v2 == 0) {
			printf("%13s", '-');
		} else {
			$ratio = $v == 0 ? $v2 : $v2/$v;
			printf("%13.6f", $ratio);
		}
	}
	printf("\n");
}


function getFiles($dir = '.') {
	$fh = opendir($dir);

	$res = array();
	$res['problems'] = array();
	while(($file = readdir($fh)) !== FALSE) {
		if ($file == '.' || $file == '..') continue;

		$parts = explode('.', $file);
		$algo = array_pop($parts);
		array_pop($parts);
		//$problem = array_pop($parts); 
		$problem = implode('.', $parts);

		$res['problems'][$problem] = $problem;
		$res[$algo][$problem] = parseResults($dir . '/' . $file);
	}

	return $res;
}

function parseResults($file) {
	$res = array();

	foreach(file($file) as $line) {
		$p = explode(' ', $line);
		$res[$p[0]] = trim($p[1]);
	}

	if (empty($res['BOUND']) && !empty($res['COST'])) {
		$res['BOUND'] = $res['COST'];
	}

	return $res;
}

