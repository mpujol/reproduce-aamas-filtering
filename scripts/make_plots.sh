#!/bin/bash

BUILD="../build"
RESULTS="../results"

[ -d "$RESULTS/rnd" ] || (echo "Unable to locate the results folder" && exit 1)

[ -d "$BUILD" ] || mkdir "$BUILD"


#----------------------------------------------------------------------
# 1. PLOTS OF RANDOM PROBLEMS (fixed domain size, increasing treewidth)
#----------------------------------------------------------------------

# 2..5 are the available domain sizes
for i in 2 3 4 5; do

	# Copy files in a suitable format
	echo "Creating folder \"$BUILD/rnd-d$i\" with problems of domain size $i ..."
	rm -rf "$BUILD/rnd-d$i"
	mkdir "$BUILD/rnd-d$i"
	for f in `find "$RESULTS/rnd" -type f -name "*maxArity=$i*" | egrep -v '(lmre-d|lmre-c|cc)$'`; do
		ln "$f" "${BUILD}/rnd-d$i/"$(basename $f | sed 's/\//_/g')
	done

	# Now dsr.php computes per-algorithm aggregated metrics between
	# all problems
	for metric in BYTES TIME; do

		# Gather statistics and filter those problems that some 
		# algorithm was unable to solve
		./dsr.php $metric "$BUILD/rnd-d$i" 2>/dev/null | grep -v ' - ' > /tmp/data.txt

		# Make the plots and latex-formatted result files
		python plotRandomData.py > /tmp/rnd-d$i-$metric.tex
		mv /tmp/graph.pdf "$BUILD/rnd-d$i-$metric.pdf"
		mv /tmp/data.txt "$BUILD/rnd-d$i-$metric.txt"
	done

	paste /tmp/rnd-d$i-BYTES.tex /tmp/rnd-d$i-TIME.tex | sed -e 's/$/\\\\/g' > "$BUILD/rnd-d$i.tex"
done


#-------------------------------------------------------------------------
# 2. PLOTS OF RANDOM PROBLEMS (fixed treewidth 10, increasing domain size)
#-------------------------------------------------------------------------

for t in BYTES TIME; do
	./dsr.php $t "$BUILD/rnd-d2" | head -n2 > /tmp/data.txt
	for f in 2 3 4 5; do
		./dsr.php $t "$BUILD/rnd-d$i" | awk '$2==10{print $1,"'$f'",$3,$4,$5,$6,$7,$8,$9,$10,$11}' | head -n35 >> /tmp/data.txt
	done
	python plotRandomEvolution.py > /tmp/$t.txt
	mv /tmp/graph.pdf "$BUILD/dmn-evolution-$t.pdf"
	cp /tmp/data.txt "$BUILD/dmn-evolution-$t.txt"
done

paste /tmp/BYTES.txt /tmp/TIME.txt | sed -e 's/$/\\\\/g' > "$BUILD/dmn-evolution.txt"
rm /tmp/BYTES.txt /tmp/TIME.txt


#-------------------------------------------------------------------------
# 2. PLOTS OF LATTICE PROBLEMS 
#-------------------------------------------------------------------------

[ ! -d "$BUILD/lattice2D" ] || rm -rf "$BUILD/lattice2D"
mkdir "$BUILD/lattice2D"
for f in `find "$RESULTS/lattice2D" -type f | egrep -v '(lmre-d|lmre-c)$'`; do
	cp $f "$BUILD/lattice2D/"$(basename $f | sed 's/\//_/g')
done
for f in BYTES TIME; do
	./dsr.php $f "$BUILD/lattice2D/" | grep -v ' - ' > /tmp/data.txt
	python plotSensorData.py
	mv /tmp/graph.pdf $BUILD/lattice-$f.pdf
	mv /tmp/data.txt $BUILD/lattice-$f.txt
done
